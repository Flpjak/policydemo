import {
    sha3withsize
} from 'solidity-sha3'
import {
    default as lightwallet
} from 'eth-lightwallet'
import {
    default as Web3
} from 'web3'
import {
    default as contract
} from 'truffle-contract'

$(document).ready(function() {
    var provider = new HookedWeb3Provider({
        host: "http://localhost:8545",
        transaction_signer: ks
    });
    window.web3 = provider;
    window.web3.setProvider(currentProvider);
    window.web3 = new Web3(currentProvider);

    Register.setProvider(web3.currentProvider)
    PolicyCreate.setProvider(web3.currentProvider)
})

window.Register = function() {
  let email = $("#email").val()
  let secretPin = $("inputSecretPin").val()
  var domain = email.replace(/.*@/, "")
  if (Register.deployed()) {
    function(contract) {
      contract.checkIfUserExists.call(userAdress).then(function(v) {
        var userCheck = v.toString()

        if (userCheck == "false") {
          $("#registerUser .next").click(function(e) {
                  $("#alertText").removeClass("out");
                  setTimeout(function(){
                    $("#wrongFormat").addClass("out");
                    $("#alreadyExists").addClass("out");
                  },500);
                })
                throw new Error()
          })
        }
        $("#inputAddress").val("")
        $("#inputSecretPin").val("")

        contract.registerVoter(userEmail, idNumber) {
          gas: 3000000,
          from: web3.eth.accounts[0]
        }).then(function() {
          $("#registerUser .next").click(function(e) {
            $('#registerUser').addClass("transition-out");
            setTimeout(function() {
              $('#registerUser').addClass("out");
              $('#policy').removeClass("out");
              setTimeout(function() {
                $('#policy').removeClass("transition-in");
              }, 100);
            }, 500);
          })
        })
      })

    }
  } else {
    function() {
      $("#registerUser .next").click(function(e) {
          $(".alertContractText").removeClass("out");
        throw new Error()
      })
    }
  }
}
