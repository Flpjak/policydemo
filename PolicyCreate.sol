pragma solidity ^0.4.10;

contract Policy {

    struct inputData {
        string name;
        string surname;
        string city;
        string physAdd;
        string zipCode;
        uint8 country;

        uint8 policyLimit;
        uint8 premium;
        uint8 riskCoverage;

        uint8 whitelisted;
    }

    struct Client {
        bytes32[] whitelisted;
    }
    inputData i;
    Client cli;
    Policy pol;


    bytes32 userEmail;
    address owner;

    function Policy(string _name, string _surname, string _city, string _phyAdd, string _zipCode,
    uint8 _country, uint8 _policyLimit, uint8 _risks, uint8 _whitelist, address _owner) {
        i.name = _name;
        i.surname = _surname;
        i.city = _city;
        i.physAdd = _phyAdd;
        i.zipCode = _zipCode;
        i.country = _country;

        i.policyLimit = _policyLimit;
        i.riskCoverage = _risks;
        i.whitelisted = _whitelist;

        owner = _owner;

    }
    modifier onlyOwner {
        require(msg.sender == owner);
        _;
    }

      function setWhitelisted(bytes32[] _emails) onlyOwner {
        for(uint i = 0; i < _emails.length; i++) {
            userEmail = _emails[i];
            cli.whitelisted.push(userEmail);
        }
    }

    function checkifWhitelisted(bytes32 email) constant returns (bool) {
        for(uint j = 0; j < cli.whitelisted.length; j++) {
            userEmail = cli.whitelisted[j];
            if (userEmail == email) {
                return true;
            }
        }
        return false;
    }

}
