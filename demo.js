
$("#start .next").click(function(e){
    $('#start').addClass("transition-out");
    setTimeout(function(){
        $('#start').addClass("out");
        $('#registerUser').removeClass("out");
        setTimeout(function(){
            $('#registerUser').removeClass("transition-in");
        },100);
    }, 500);
})

$(".alert .close").click(function(e){
  setTimeout(function(){
    $(".alert-warning").addClass("out");
  },100)
})

$("#policy .next").click(function(e){
    $('#policy').addClass("transition-out");
    setTimeout(function(){
        $('#policy').addClass("out");
        $('#coverage').removeClass("out");
        setTimeout(function(){
            $('#coverage').removeClass("transition-in");
        },100);
    }, 500);
});
