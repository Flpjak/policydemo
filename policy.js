import {
    sha3withsize
} from 'solidity-sha3'
import {
    default as lightwallet
} from 'eth-lightwallet'
import {
    default as Web3
} from 'web3'
import {
    default as HookedWeb3Provider
} from 'hooked-web3-provider'
import {
    default as contract
} from 'truffle-contract'

$(document).ready(function() {
    var provider = new HookedWeb3Provider({
        host: "http://localhost:8545",
        transaction_signer: ks
    });
    window.web3 = provider;
    window.web3.setProvider(currentProvider);
    window.web3 = new Web3(currentProvider);

    Register.setProvider(web3.currentProvider)
    PolicyCreate.setProvider(web3.currentProvider)
})


  let email = $("#inputAddress").val()

  contract.checkifWhitelisted.call(email).then(function(v) {
    let checkWhitelisted = v.toString()
    if (checkWhitelisted !== email) {
      if (userCheck == "false") {
        $("#coverage .next").click(function(e) {
          $("#alertNotWhitelisted").removeClass("out");
        })
        throw new Error()
      }
    } else {
      $("#coverage .next").click(function(e){

      })

      let name = $("#name").val()
      let surname = $("#surname").val()
      let city = $("#city").val()
      let address = $("#address").val()
      let zip = $("#zip").val()
      let country = $("#select[class=custom-select d-block]").val()

      let limit = $("#limit").val()
      let premium = $("#premium").val()
      let risks = $("#vtitle").val()
      let ballottype = $("input[name=ballottype]:checked").val()
      let whitelisted = $("#whitelisted").val()
      var whitelistedArray = whitelisted.split(/\s*,\s*/)
      $('#coverage').addClass("transition-out");
      setTimeout(function(){
          $('#coverage').addClass("out");
          $('#end').removeClass("out");
          setTimeout(function(){
              $('#end').removeClass("transition-in");
          },100);
      }, 500);
    }
  })
}
